from django import forms


class ScheduleMappingsSearchForm(forms.Form):
    all_search = forms.CharField(label='Search All Mapping Fields', max_length=100)


class ScheduleMappingsFieldSearchForm(forms.Form):
    field = forms.CharField(label='Field', max_length=100)
    search = forms.CharField(label='Search Text', max_length=100)


class ScheduleDisplayFieldForm(forms.Form):
    field = forms.CharField(label='Field', max_length=100)


class CredentialsForm(forms.Form):
    user = forms.CharField(label='User', max_length=100)
    password = forms.CharField(label='Password', max_length=100, widget=forms.PasswordInput())
    hostname = forms.CharField(label='Hostname', max_length=100)
    port = forms.IntegerField(label='Port')
