from django.db import models
from fernet_fields import EncryptedTextField
import uuid



class Credentials(models.Model):
    user = models.CharField(max_length=30)
    password = EncryptedTextField()
    hostname = models.CharField(max_length=30)
    port = models.IntegerField()
    ident = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
