from django.http import HttpResponse, HttpResponseRedirect
from django.views import View
from ..forms import CredentialsForm
from lib.credentials import CredentialsToken
from lib import api_util
from ..models import Credentials
from django.template import loader
from django.contrib.auth import authenticate
from django.urls import reverse
from lib.credentials import CredentialException


cred_label = 'credentials'


class CredView(View):
    def dispatch(self, request):
        if not request.user.is_authenticated:
            return HttpResponseRedirect(reverse('login'))
        if cred_label not in request.session:
            return credentials(request)
        return View.dispatch(self, request)

    @staticmethod
    def get_api_results(request, command):
        if cred_label in request.session:
            cred = get_selected_credentials(request.session)
            try:
                return api_util.get_api_results(cred.user, cred.password, cred.hostname, cred.port, command)
            except CredentialException:
                return credentials(request)
        # Get credentials if not selected
        return credentials(request)

    @staticmethod
    def delete_api_results(request, command):
        if cred_label in request.session:
            cred = get_selected_credentials(request.session)
            return api_util.delete_api_results(cred.user, cred.password, cred.hostname, cred.port, command)
        # Get credentials if not selected
        return credentials(request)


def credentials(request):
    cred = None
    if request.method == 'POST':
        cred_form = CredentialsForm(request.POST)
        if cred_form.is_valid():
            if 'user' in cred_form.cleaned_data and 'password' in cred_form.cleaned_data and \
                            'hostname' in cred_form.cleaned_data and 'port' in cred_form.cleaned_data:
                user = cred_form.cleaned_data['user']
                password = cred_form.cleaned_data['password']
                hostname = cred_form.cleaned_data['hostname']
                port = cred_form.cleaned_data['port']

                cred = Credentials.objects.create(user=user, password=password, hostname=hostname, port=port)

    credential_list = Credentials.objects.all()

    credentials_form = CredentialsForm()

    display_cred = get_selected_credentials(request.session)

    template = loader.get_template('schedules_filter/credentials.html')
    context = {
        'credential_list': credential_list,
        'credentials_form': credentials_form,
        'display_cred': display_cred,
    }
    return HttpResponse(template.render(context, request))


def credentials_select(request, cred_id):
    request.session[cred_label] = cred_id

    credential_list = Credentials.objects.all()

    credentials_form = CredentialsForm()

    display_cred = get_selected_credentials(request.session)

    template = loader.get_template('schedules_filter/credentials.html')
    context = {
        'credential_list': credential_list,
        'credentials_form': credentials_form,
        'display_cred': display_cred,
    }
    return HttpResponse(template.render(context, request))


def credentials_delete(request, cred_id):
    cred = Credentials.objects.get(pk=cred_id)

    cred.delete()

    credential_list = Credentials.objects.all()

    credentials_form = CredentialsForm()

    display_cred = get_selected_credentials(request.session)

    template = loader.get_template('schedules_filter/credentials.html')
    context = {
        'credential_list': credential_list,
        'credentials_form': credentials_form,
        'display_cred': display_cred,
    }
    return HttpResponse(template.render(context, request))


def get_selected_credentials(session):
    if cred_label in session:
        try:
            cred = Credentials.objects.get(pk=session[cred_label])
        except Exception:
            del session[cred_label]
            return CredentialsToken()
        cred_token = CredentialsToken.create(cred.user, cred.password, cred.hostname, cred.port, str(cred.ident))
        display_cred = cred_token
    else:
        display_cred = CredentialsToken()
    return display_cred
