from .credentials import CredView, cred_label
from ..forms import ScheduleMappingsSearchForm, ScheduleMappingsFieldSearchForm, ScheduleDisplayFieldForm
from lib.schedule_search import match_any_mapping, match_mapping_field
from django.urls import reverse
from .credentials import CredView
from .credentials import get_selected_credentials
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from json2html import *
from schedules_filter.lib.processes import list_process_id


all_search_label = 'all_search'
field_search_label = 'field_search'
display_field_label = 'display_field'
schedule_filters_label = 'schedule_filters'


class Schedules(CredView):
    @staticmethod
    def get(request):
        return schedules(request)


class SchedulesAllSearch(CredView):
    @staticmethod
    def post(request):
        return schedules_all_search(request)


class SchedulesFieldSearch(CredView):
    @staticmethod
    def post(request):
        return schedules_field_search(request)


class SchedulesDisplayField(CredView):
    @staticmethod
    def post(request):
        return display_field(request)


class SchedulesClear(CredView):
    @staticmethod
    def post(request):
        return schedules_clear(request)


def get_cleared_schedule_filter():
    return {all_search_label: [], field_search_label: [], display_field_label: []}


def get_schedule_filter(request, label):
    init_schedule_filters_if_not_exist(request)
    print("GET")
    print(request.session[schedule_filters_label])
    return request.session[schedule_filters_label][label]
#    return request.session[label]


def append_schedule_filter(request, label, array):
    init_schedule_filters_if_not_exist(request)
    hold = request.session[schedule_filters_label]
    hold[label] = array
    request.session[schedule_filters_label] = hold


def clear_schedule_filters(request):
    print("Clear")
    request.session[schedule_filters_label] = get_cleared_schedule_filter()


def init_schedule_filters_if_not_exist(request):
    if schedule_filters_label not in request.session:
        clear_schedule_filters(request)


def schedules(request):
    search_filter = ''
    response = CredView.get_api_results(request, "synic/api/scheduler/schedule")
    if type(response) == HttpResponse:
        return response
    template = loader.get_template('schedules_filter/schedules.html')

    search_filter_array = []
#    if all_search_label in request.session:
    search_filter_array.append("Any Mapping Value containing {}".format(get_schedule_filter(request, all_search_label)))
    for a_s in get_schedule_filter(request, all_search_label):
        response = list(filter(lambda s: match_any_mapping(a_s, s), response))

#    if field_search_label in request.session:
    search_filter_array.append("Field:Value Mapping containing {}".format(get_schedule_filter(request, field_search_label)))
    for f_s in get_schedule_filter(request, field_search_label):
        response = list(filter(lambda s: match_mapping_field(f_s['field'], f_s['search'], s), response))

    display_fields = []
    search_filter_array.append("Additional Display Fields {}".format(get_schedule_filter(request, display_field_label)))
    for d_f in get_schedule_filter(request, display_field_label):
        field_parts = str(d_f).split('.')
        for s in response:
            field = s
            for part in field_parts:
                if part in field:
                    field = field[part]
                else:
                    field = ''
                    break
            if 'display_fields' not in s:
                s['display_fields'] = []
            s['display_fields'].append(field)
    # Load Number of Processes
    for s in response:
        processes = list_process_id(s)
        s['process_count'] = len(processes)

    search_filter = "<br>".join(search_filter_array)

    display_cred = get_selected_credentials(request.session)

    response = sorted(response, key=lambda k: k['id'])
    response = sorted(response, key=lambda k: k['templateName'])
    for i in range(len(display_fields)):
        response = sorted(response, key=lambda k: k['display_fields'][i])
    all_search = ScheduleMappingsSearchForm()
    field_search = ScheduleMappingsFieldSearchForm()
    display_field = ScheduleDisplayFieldForm()
    context = {
        'schedules_json': response,
        'all_search': all_search,
        'field_search': field_search,
        'display_field': display_field,
        'display_fields': display_fields,
        'search_filter': search_filter,
        'display_cred': display_cred,
    }
    return HttpResponse(template.render(context, request))


def schedules_all_search(request):
    if request.method == 'POST':
        all_search_form = ScheduleMappingsSearchForm(request.POST)
        if all_search_form.is_valid():
            if all_search_label in all_search_form.cleaned_data:
                all_search_value = all_search_form.cleaned_data[all_search_label]
                all_search_array = get_schedule_filter(request, all_search_label)
                all_search_array.append(all_search_value)
                append_schedule_filter(request, all_search_label, all_search_array)
    return schedules(request)


def schedules_field_search(request):
    if request.method == 'POST':
        field_search_form = ScheduleMappingsFieldSearchForm(request.POST)
        if field_search_form.is_valid():
            if 'field' in field_search_form.cleaned_data and 'search' in field_search_form.cleaned_data:
                field_value = field_search_form.cleaned_data['field']
                search_value = field_search_form.cleaned_data['search']

                field_search_array = get_schedule_filter(request, field_search_label)
                field_search_array.append({'field': field_value, 'search': search_value})
                append_schedule_filter(request, field_search_label, field_search_array)

    return schedules(request)


def display_field(request):
    if request.method == 'POST':
        display_field_form = ScheduleDisplayFieldForm(request.POST)
        # check whether it's valid:
        if display_field_form.is_valid():
            if 'field' in display_field_form.cleaned_data:
                field = display_field_form.cleaned_data['field']
                display_field_array = get_schedule_filter(request, display_field_label)
                display_field_array.append(field)
                append_schedule_filter(request, display_field_label, display_field_array)

    return schedules(request)


def schedules_clear(request):
    if request.method == 'POST':
        clear_schedule_filters(request)
    return HttpResponseRedirect(reverse('schedules'))
