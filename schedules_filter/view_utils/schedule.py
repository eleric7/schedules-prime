from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from json2html import *
from lib.api_util import get_api_results
from .credentials import CredView
from schedules_filter.lib.processes import process_summary_list_for_schedule
from django.urls import reverse


class ScheduleID(CredView):

    def dispatch(self, request, schedule_id):
        return CredView.dispatch(self=self, request=request)


class Schedule(ScheduleID):

    def get(self, request):
        return schedule(request, self.kwargs['schedule_id'])


class ScheduleMapping(ScheduleID):

    def get(self, request):
        return schedule_mappings(request, self.kwargs['schedule_id'])


class ScheduleResources(ScheduleID):

    def get(self, request):
        return schedule_resources(request, self.kwargs['schedule_id'])


class ScheduleConditionalAction(ScheduleID):

    def get(self, request):
        return schedule_conditional_action(request, self.kwargs['schedule_id'])


class ScheduleSuccessExecutions(ScheduleID):

    def get(self, request):
        return schedule_successful_executions(request, self.kwargs['schedule_id'])


class ScheduleErrorMessage(ScheduleID):

    def get(self, request):
        return schedule_error_message(request, self.kwargs['schedule_id'])


class ScheduleErrorStackTrace(ScheduleID):

    def get(self, request):
        return schedule_error_stack_trace(request, self.kwargs['schedule_id'])


class ScheduleProcesses(ScheduleID):

    def get(self, request):
        return schedule_process(request, self.kwargs['schedule_id'])


def empty_if_none(obj):
    if obj:
        return obj
    else:
        return "<p>Empty</p>"


def schedule(request, schedule_id):
    response = CredView.get_api_results(request, "synic/api/scheduler/schedule")
    for element in response:
        if "id" in element:
            if element["id"] == schedule_id:
                proc_list = process_summary_list_for_schedule(request, element)
                template = loader.get_template('schedules_filter/schedule.html')
                if 'mappings' in element:
                    element['mappings'] = OrderedDict(sorted(element['mappings'].items()))
                if 'resources' in element:
                    element['resources'] = OrderedDict(sorted(element['resources'].items()))
                context = {
                    'schedule_json': element,
                    'mappings': empty_if_none(json2html.convert(json=element['mappings'])),
                    'resources': empty_if_none(json2html.convert(json=element['resources'])),
                    'conditionalAction': empty_if_none(json2html.convert(json=element['conditionalAction'])),
                    'successfulExecutions': empty_if_none(json2html.convert(json=element['successfulExecutions'])),
                    'errorMessage': empty_if_none(json2html.convert(json=element['errorMessage'])),
                    'errorStackTrace': empty_if_none(json2html.convert(json=element['errorStackTrace'])),
                    'processes': empty_if_none(json2html.convert(json=proc_list)),
                }
                return HttpResponse(template.render(context, request))
    return HttpResponse("Schedule ID not found")


def schedule_mappings(request, schedule_id):
    response = CredView.get_api_results(request, "synic/api/scheduler/schedule")
    for element in response:
        if "id" in element:
            if element["id"] == schedule_id:
                template = loader.get_template('schedules_filter/schedule.html')
                if 'mappings' in element:
                    element['mappings'] = OrderedDict(sorted(element['mappings'].items()))
                context = {
                    'schedule_json': element,
                    'mappings': empty_if_none(json2html.convert(json=element['mappings'])),
                }
                return HttpResponse(template.render(context, request))
    return HttpResponse("Schedule ID not found")


def schedule_resources(request, schedule_id):
    response = CredView.get_api_results(request, "synic/api/scheduler/schedule")
    for element in response:
        if "id" in element:
            if element["id"] == schedule_id:
                template = loader.get_template('schedules_filter/schedule.html')
                if 'resources' in element:
                    element['resources'] = OrderedDict(sorted(element['resources'].items()))
                context = {
                    'schedule_json': element,
                    'resources': empty_if_none(json2html.convert(json=element['resources'])),
                }
                return HttpResponse(template.render(context, request))
    return HttpResponse("Schedule ID not found")


def schedule_conditional_action(request, schedule_id):
    response = CredView.get_api_results(request, "synic/api/scheduler/schedule")
    for element in response:
        if "id" in element:
            if element["id"] == schedule_id:
                template = loader.get_template('schedules_filter/schedule.html')
                context = {
                    'schedule_json': element,
                    'conditionalAction': empty_if_none(json2html.convert(json=element['conditionalAction'])),
                }
                return HttpResponse(template.render(context, request))
    return HttpResponse("Schedule ID not found")


def schedule_successful_executions(request, schedule_id):
    response = CredView.get_api_results(request, "synic/api/scheduler/schedule")
    for element in response:
        if "id" in element:
            if element["id"] == schedule_id:
                template = loader.get_template('schedules_filter/schedule.html')
                context = {
                    'schedule_json': element,
                    'successfulExecutions': empty_if_none(json2html.convert(json=element['successfulExecutions'])),
                }
                return HttpResponse(template.render(context, request))
    return HttpResponse("Schedule ID not found")


def schedule_error_message(request, schedule_id):
    response = CredView.get_api_results(request, "synic/api/scheduler/schedule")
    for element in response:
        if "id" in element:
            if element["id"] == schedule_id:
                template = loader.get_template('schedules_filter/schedule.html')
                context = {
                    'schedule_json': element,
                    'errorMessage': empty_if_none(json2html.convert(json=element['errorMessage'])),
                }
                return HttpResponse(template.render(context, request))
    return HttpResponse("Schedule ID not found")


def schedule_error_stack_trace(request, schedule_id):
    response = CredView.get_api_results(request, "synic/api/scheduler/schedule")
    for element in response:
        if "id" in element:
            if element["id"] == schedule_id:
                template = loader.get_template('schedules_filter/schedule.html')
                context = {
                    'schedule_json': element,
                    'errorStackTrace': empty_if_none(json2html.convert(json=element['errorStackTrace'])),
                }
                return HttpResponse(template.render(context, request))
    return HttpResponse("Schedule ID not found")


def schedule_process(request, schedule_id):
    response = CredView.get_api_results(request, "synic/api/scheduler/schedule")
    for element in response:
        if "id" in element:
            if element["id"] == schedule_id:
                proc_list = process_summary_list_for_schedule(request, element)
                template = loader.get_template('schedules_filter/schedule.html')
                context = {
                    'schedule_json': element,
                    'processes': empty_if_none(json2html.convert(json=proc_list)),
                }
                return HttpResponse(template.render(context, request))
    return HttpResponse("Schedule ID not found")

