from django.urls import path
from .view_utils.schedules import Schedules, SchedulesAllSearch, SchedulesClear, SchedulesDisplayField, SchedulesFieldSearch
from .view_utils.schedule import Schedule, ScheduleMapping, ScheduleResources, ScheduleConditionalAction, ScheduleSuccessExecutions, ScheduleErrorMessage, ScheduleErrorStackTrace, ScheduleProcesses
from .view_utils import schedules
from .view_utils import schedule
from django.contrib.auth import views as auth_views
from django.contrib import admin
from django.conf.urls import url
from .view_utils import credentials

urlpatterns = [
    path('', Schedules.as_view(), name='index'),
    # Schedule mapping
    path('schedule/<str:schedule_id>', Schedule.as_view(), name='schedule'),
    path('schedule/mappings/<str:schedule_id>', ScheduleMapping.as_view(), name='schedule_mappings'),
    path('schedule/resources/<str:schedule_id>', ScheduleResources.as_view(), name='schedule_resources'),
    path('schedule/conditional_action/<str:schedule_id>', ScheduleConditionalAction.as_view(), name='schedule_conditional_action'),
    path('schedule/successful_executions/<str:schedule_id>', ScheduleSuccessExecutions.as_view(), name='schedule_successful_executions'),
    path('schedule/error_message/<str:schedule_id>', ScheduleErrorMessage.as_view(), name='schedule_error_message'),
    path('schedule/error_stack_trace/<str:schedule_id>', ScheduleErrorStackTrace.as_view(), name='schedule_error_stack_trace'),
    path('schedule/processes/<str:schedule_id>', ScheduleProcesses.as_view(), name='schedule_processes'),
    # Schedules mappings
    path('schedules', Schedules.as_view(), name='schedules'),
    path('schedules/clear', SchedulesClear.as_view(), name='clear_filters'),
    path('schedules/all_search', SchedulesAllSearch.as_view(), name='add_search_all_filter'),
    path('schedules/field_search', SchedulesFieldSearch.as_view(), name='add_search_field_filter'),
    path('schedules/display_field', SchedulesDisplayField.as_view(), name='add_display_field'),
    # Credentials mapping
    path('schedules/credentials', credentials.credentials, name='credentials'),
    path('schedules/credentials/select/<str:cred_id>', credentials.credentials_select, name='credentials_select'),
    path('schedules/credentials/delete/<str:cred_id>', credentials.credentials_delete, name='credentials_delete'),
    # Login
    path('login', auth_views.login, name='login'),
    path('logout', auth_views.logout, {'next_page': 'schedules'}, name='logout'),

]