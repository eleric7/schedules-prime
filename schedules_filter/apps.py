from django.apps import AppConfig


class SchedulesFilterConfig(AppConfig):
    name = 'schedules_filter'
