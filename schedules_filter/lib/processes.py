from schedules_filter.view_utils.credentials import CredView

def list_process_id_name(sched):
    process_name_id_list = []
    if 'resources' in sched:
        resources = sched['resources']
        for resource in resources:
            if str(resource) != 'current' and str(resources[resource]).startswith('/process/$'):
                process_name_id_list.append(str(resources[resource]).replace('/process/$', ''))
    return process_name_id_list


def list_process_id(sched):
    names = list_process_id_name(sched)
    id_list = []
    if 'mappings' in sched:
        mappings = sched['mappings']
        for name in names:
            if name in mappings:
                id_list.append(mappings[name])
    return id_list


def process_list_for_schedule(request, sched):
    proc_list = []
    proc_id_list = list_process_id(sched)
    for proc_id in proc_id_list:
        proc = process_by_id(request, proc_id)
        proc_list.append(proc)
    return proc_list


def process_by_id(request, proc_id):
    response = CredView.get_api_results(request, "synic/api/process/{}".format(proc_id))
    return response


def process_summary_list_for_schedule(request, sched):
    proc_list = process_list_for_schedule(request, sched)
    proc_summary_list = []
    for proc in proc_list:
        proc_summary = {}
        for field in ('id', 'kb', 'application', 'processType', 'username', 'requestedTime', 'startedTime', 'completedTime', 'status'):
            if field in proc:
                proc_summary[field] = proc[field]
        proc_summary_list.append(proc_summary)
    proc_summary_list = sorted(proc_summary_list, key=lambda k: k['requestedTime'])
    return proc_summary_list
