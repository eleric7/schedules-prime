

def match_any_mapping(all_search, sched):
    if 'mappings' in sched:
        mappings = sched['mappings']
        for key, value in mappings.items():
            if all_search in str(value):
                return True
    return False


def match_mapping_field(field, search, sched):
    if 'mappings' in sched:
        mappings = sched['mappings']
        if field in mappings:
            if search in str(mappings[field]):
                return True
    return False
