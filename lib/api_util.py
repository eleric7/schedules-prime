import requests
from json.decoder import JSONDecodeError
from lib.credentials import CredentialException


def get_request(command, user, password):
    r = requests.get(command, auth=(user, password))
    try:
        return r.json()
    except JSONDecodeError:
        print("Excp")
        raise CredentialException


# def delete_request(command, user, password):
#     r = requests.delete(command, auth=(user, password))
#     return r.json()


def get_api_results(user, password, hostname, port, api_command):
    command = "http://{0}:{1}/{2}".format(hostname, port, api_command)

    out = get_request(command, user, password)
    return out


# def delete_api_results(user, password, hostname, port, api_command):
#     command = "http://{0}:{1}/{2}".format(hostname, port, api_command)
#
#     out = delete_request(command, user, password)
#     return out

# def get_api_results_with_config(api_command):
#    return get_api_results("svcesvr_synthesis_de", "XXXXXX", "devesvrcn01", "9003", api_command)
