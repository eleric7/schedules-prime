import json


def check_credentials(request):
    print("Credentials")


class CredentialsToken(object):
    user = None
    password = None
    hostname = None
    port = None
    ident = None

    def to_json(self):
        return json.dumps({'user': self.user, 'password': self.password, 'hostname': self.hostname, 'port': int(self.port), 'indent': self.ident})

    @staticmethod
    def from_json(json_str):
        cred_json = json.loads(json_str)
        cred = CredentialsToken()
        if 'user' in cred_json:
            cred.user = cred_json['user']
        if 'password' in cred_json:
            cred.password = cred_json['password']
        if 'hostname' in cred_json:
            cred.hostname = cred_json['hostname']
        if 'port' in cred_json:
            cred.port = cred_json['port']
        if 'ident' in cred_json:
            cred.ident = cred_json['ident']
        return cred

    @staticmethod
    def create(user, password, hostname, port, ident):
        cred = CredentialsToken()
        cred.user = user
        cred.password = password
        cred.hostname = hostname
        cred.port = port
        cred.ident = ident
        return cred


class CredentialException(Exception):
    pass